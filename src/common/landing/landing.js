import 'angular';
// import 'fullpage.js/jquery.fullPage.js';
// import 'angular-fullpage.js';
// import 'odometer';
// import 'angular-odometer-js';
import 'angular-scroll-animate';
import 'animate.css';
import './landing.scss';

import template from './landing.html';

import './../../components/ng-smooth-scroll/ng-smooth-scroll';

import headshot from './headshot.jpeg';

class LandingController {
    constructor($timeout) {
        this.$timeout = $timeout;

        this.headshot = headshot;
    }

    fadeIn($el) {
        $el.addClass('animated fadeIn');
    }
}

const landingComponent = {
    template: template,
    controller: LandingController
};

const stateConfig = ($stateProvider) => {
    const landing = {
        name: 'app.landing',
        url: '/',
        template: '<landing></landing>'
    };

    $stateProvider
        .state(landing);
};

const run = ($window) => {
    $window.$ = $;
}

const landing = angular
    .module('landing', [
        // 'fullPage.js',
        // 'ui.odometer'
        'smoothScroll',
        'angular-scroll-animate'
    ])
    .component('landing', landingComponent)
    .config(stateConfig)
    .run(run)
    .directive('ngScrollClass', function ($window) {
      var $win = angular.element($window); // wrap window object as jQuery object

      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var topClass = attrs.ngScrollClass, // get CSS class from directive's attribute value
              offsetTop = element.offset().top; // get element's offset top relative to document

          $win.on('scroll', function (e) {
            if ($win.scrollTop() >= offsetTop) {
              element.addClass(topClass);
            } else {
              element.removeClass(topClass);
            }
          });
        }
      };
    })
    .name;

export default landing;
